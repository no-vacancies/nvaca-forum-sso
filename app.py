import sys
import re
import uuid
from datetime import datetime, timedelta

from esipy import EsiApp, EsiClient, EsiSecurity
from esipy.cache import FileCache
from esipy.exceptions import APIException

from flask import Flask, redirect, request, session, url_for, make_response
from flask_login import LoginManager, UserMixin, current_user, logout_user, login_required, login_user

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.exc import NoResultFound

import config
import time
import hashlib
import requests

app = Flask(__name__)
app.config.from_object(config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


class User(db.Model, UserMixin):
    character_id = db.Column(
        db.BigInteger,
        primary_key=True,
        autoincrement=False
    )
    character_owner_hash = db.Column(db.String(255))
    character_name = db.Column(db.String(200))

    access_token = db.Column(db.String(100))
    access_token_expires = db.Column(db.DateTime())
    refresh_token = db.Column(db.String(100))

    flarum_user_id = db.Column(db.Integer)
    flarum_password = db.Column(db.String(255))
    flarum_email = db.Column(db.String(255))

    create_time = db.Column(db.DateTime())

    @property
    def forum_name(self):
        return re.sub(r'\W+','', self.character_name)

    def get_id(self):
        return self.character_id

    def get_sso_data(self):
        return {
            'access_token': self.access_token,
            'refresh_token': self.refresh_token,
            'expires_in': (self.access_token_expires - datetime.utcnow()).total_seconds()
        }

    def update_token(self, token_response):
        self.access_token = token_response['access_token']
        self.access_token_expires = datetime.fromtimestamp(time.time() + token_response['expires_in'])
        if 'refresh_token' in token_response:
            self.refresh_token = token_response['refresh_token']


@login_manager.user_loader
def load_user(character_id):
    return User.query.get(character_id)


esi_app = EsiApp()
esiapp = esi_app.get_latest_swagger
cache = FileCache(path='.webcache')

esisecurity = EsiSecurity(
    #app=esiapp,
    sso_url="https://login.eveonline.com/v2",
    headers={'User-Agent': config.ESI_USER_AGENT},
    redirect_uri=config.ESI_CALLBACK,
    client_id=config.ESI_CLIENT_ID,
    secret_key=config.ESI_SECRET_KEY,
)

esiclient = EsiClient(
    security=esisecurity,
    cache=cache,
    retry_requests=True,
    headers={'User-Agent': config.ESI_USER_AGENT}
)


def get_forum_user_auth_header(token):
    """
    Generate a forum auth header from a token response.
    :param token: Result of get_forum_token or dict with token and userId
    :return: Authorization header with the token and user id.
    """
    header = {'Authorization': 'Token {token}; userId={userId}'.format(**token)}
    return header


def get_forum_admin_headers():
    """
    Generate admin forum auth. Root token defined in config.
    :return: Authorization header with userId 1 and root token.
    """
    return get_forum_user_auth_header({'token': config.FLARUM_ROOT_TOKEN, 'userId': '1'})


def register_forum_user(user):
    """
    Register a new user on the forums
    :param user: User model with generate password and email
    :return: Result of the forum api call
    """
    uri = config.FLARUM_ROOT_URI + '/api/users'
    data = {
        'data': {
            'attributes': {
                'username': user.forum_name,
                'password': user.flarum_password,
                'email': user.flarum_email,
            },
        },
    }
    headers = get_forum_admin_headers()
    result = requests.post(uri, headers=headers, json=data)
    return result


def activate_forum_user(user):
    """
    Activate the forum user corresponding to a passed User object
    :param user:
    :return: Result of the forum api call
    """
    uri = config.FLARUM_ROOT_URI + '/api/users/{}'.format(user.flarum_user_id)
    data = {
        'data': {
            'attributes': {
                'isActivated': True,
            },
        },
    }
    headers = get_forum_admin_headers()
    result = requests.patch(uri, headers=headers, json=data)
    return result


def get_forum_group_ids():
    """
    Get a list of all forum groups and their IDs
    :return: Dictonary using the pattern `{'groupname': id}`.
    """
    uri = config.FLARUM_ROOT_URI + '/api/groups'
    result = requests.get(uri, headers=get_forum_admin_headers())
    group_dict = {g['attributes']['nameSingular']: g['id'] for g in result.json()['data']}
    return group_dict


def get_forum_token(user):
    """
    Get a forum auth token for the passed user.
    :param user: User model.
    :return: API response if auth successful, None otherwise.
    """
    uri = config.FLARUM_ROOT_URI + '/api/token'
    data = {
        'identification': user.forum_name,
        'password': user.flarum_password,
    }
    result = requests.post(uri, data=data)
    if result.status_code < 400:
        return result
    else:
        return None


def get_forum_user(user):
    """
    Get the forum data for a passed user
    :param user: User model
    :return: Dict with the user data.
    """
    uri = config.FLARUM_ROOT_URI + '/api/users/{userId}'.format(userId=user.flarum_user_id)
    result = requests.get(uri, headers=get_forum_admin_headers())
    return result.json()


def set_forum_user(data):
    """
    Update a forum user
    :param data:
    :return:
    """
    uri = config.FLARUM_ROOT_URI + '/api/users/{id}'.format(**data['data'])
    result = requests.patch(uri, headers=get_forum_admin_headers(), json=data)
    return result


def add_forum_group(user, group_name):
    """
    Add an existing forum group to an existing forum user
    :param user: User model
    :param group_name: Singular name of the forum group
    :return: API response
    """
    forum_user = get_forum_user(user)
    groups = get_forum_group_ids()
    user_groups = forum_user['data']['relationships']['groups']['data']
    if group_name in groups.keys():
        user_groups.append({'type': 'group', 'id': groups[group_name]})
    forum_user['data']['relationships']['groups']['data'] = user_groups
    result = set_forum_user(forum_user)
    return result


def delete_forum_group(user, group_name):
    """
    Remove a group from an existing forum user
    :param user: User model
    :param group_name: Singular name of the forum group
    :return: API response
    """
    forum_user = get_forum_user(user)
    groups = get_forum_group_ids()
    user_groups = forum_user['data']['relationships']['groups']['data']
    user_groups = [g for g in user_groups if g.get('id') != groups.get(group_name)]
    forum_user['data']['relationships']['groups']['data'] = user_groups

    result = set_forum_user(forum_user)

    return result


def get_character_data(user):
    """
    Get public character data from ESI
    :param user: User model
    :return: ESI Data
    """
    esisecurity.update_token(user.get_sso_data())
    op = esiapp.op['get_characters_character_id'](character_id=user.character_id)
    data = esiclient.request(op)

    return data


@app.route('/sso/login')
def login():
    """
    Redirect to EVE SSO
    :return: redirect to EVE SSO Login url.
    """
    nonce = uuid.uuid4().hex
    session["nonce"] = nonce
    return redirect(esisecurity.get_auth_uri(
        scopes=['esi-characters.read_corporation_roles.v1'],
        state=nonce
    ))


@app.route('/sso/logout')
@login_required
def logout():
    """
    Log out user from Forum and auth.
    :return: redirect to forum landing page
    """
    logout_user()
    resp = make_response(redirect(config.FLARUM_ROOT_URI))
    resp.set_cookie('flarum_remember', '', expires=0)
    return resp


@app.route('/sso/callback')
def callback():
    """
    Handle SSO Response, log in to auth and forum.
    :return:
    """
    code = request.args.get('code')

    try:
        auth_response = esisecurity.auth(code)
    except APIException as e:
        return 'Login EVE Online SSO failed: {}'.format(e), 403

    cdata = esisecurity.verify()

    if current_user.is_authenticated:
        logout_user()

    try:
        user = User.query.filter(
            User.character_id == cdata['CharacterID']
        ).one()
    except NoResultFound:
        user = User()
        user.character_id = cdata['CharacterID']

    # TODO: Deal with character owner change.
    user.character_owner_hash = cdata['CharacterOwnerHash']
    user.character_name = cdata['CharacterName']
    user.update_token(auth_response)

    token = get_forum_token(user)

    # TODO: Handle User existing in Flarum but not flask-login.
    # get user id via name, update password etc with root token.

    if not token:
        # we'll assume no forum user exists. This needs some more checks to avoid collisions.
        user.flarum_email = config.FLARUM_EMAIL_PATTERN.format(
            user=user.forum_name.lower(),
            character_id=user.character_id)
        user.flarum_password = hashlib.sha256('{}{}'.format(user.character_id, time.time()).encode()).hexdigest()
        forum_user = register_forum_user(user)
        if forum_user.status_code == 201:
            user.flarum_user_id = forum_user.json()['data']['id']
            forum_activate = activate_forum_user(user)
            # TODO: Error handling if no user created

    token = get_forum_token(user)

    try:
        character_data = get_character_data(user)

        for corp_id, group_name in config.CORP_WHITELIST:
            if character_data.data.corporation_id == corp_id:
                add_forum_group(user, group_name)
            else:
                delete_forum_group(user, group_name)
    except Exception as e:
        # Temporary workaround to allow login when ESI is down
        # TODO: Improve this by addin proper logging
        print(e)
        pass

    try:
        db.session.merge(user)
        db.session.commit()

        login_user(user)
        session.permanent = True
        expires = datetime.utcnow() + timedelta(days=30)
        res = redirect(config.FLARUM_ROOT_URI)
        res.set_cookie('flarum_remember', token.json()['token'], expires=expires)
        return res

    except:
        print(sys.exc_info())
        db.session.rollback()
        logout_user()

    return redirect(config.FLARUM_ROOT_URI)

if __name__ == '__main__':
    app.run(port=config.PORT, host=config.HOST)
