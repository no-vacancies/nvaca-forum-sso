<?php

use Nvaca\Listener;
use Illuminate\Contracts\Events\Dispatcher;

return function (Dispatcher $events) {
	$events->subscribe(Listener\AddEveAvatarAssets::class);
	$events->subscribe(Listener\AddUserEmailAttributes::class);
};
