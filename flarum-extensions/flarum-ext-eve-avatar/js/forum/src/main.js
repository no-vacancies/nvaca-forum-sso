import User from 'flarum/models/User';

app.initializers.add('nvaca-eveavatar', function() {
    User.prototype.avatarUrl = function() {
        var user = this;
        var avatar_url = user.attribute('avatarUrl');

        if (!avatar_url) {
            var email = user.attribute('email');
            if (email) {
                var email_local = email.split('@')[0];
                var charid = email_local.split('+').slice(-1)[0];

                user.pushAttributes({
                    avatarUrl: 'https://image.eveonline.com/Character/'+charid+'_256.jpg',
                    avatarColor: null
                });
            }
        }

        return user.attribute('avatarUrl');
    };

    User.prototype.calculateAvatarColor = function() {
        const image = new Image();
        const user = this;

        image.onload = function() {
            const colorThief = new ColorThief();
            user.avatarColor = colorThief.getColor(this);
            user.freshness = new Date();
            m.redraw();
        };
        // https://github.com/lokesh/color-thief/issues/20
        image.crossOrigin = 'Anonymous';
        image.src = this.avatarUrl();
    };
});
