<?php

namespace Nvaca\Listener;

use Flarum\Api\Serializer\UserBasicSerializer;
use Flarum\Event\PrepareApiAttributes;
use Illuminate\Contracts\Events\Dispatcher;

class AddUserEmailAttributes
{
    public function subscribe(Dispatcher $events)
    {
        $events->listen(PrepareApiAttributes::class, [$this, 'addEmailAttributes']);
    }

    public function addEmailAttributes(PrepareApiAttributes $event)
    {
        if ($event->isSerializer(UserBasicSerializer::class)) {
            $event->attributes['email'] = $event->model->email;
        }
    }
}
